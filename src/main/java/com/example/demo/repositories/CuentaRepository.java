package com.example.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.entities.CuentaEntity;

@Repository
public interface CuentaRepository extends CrudRepository<CuentaEntity, Long>{

}
