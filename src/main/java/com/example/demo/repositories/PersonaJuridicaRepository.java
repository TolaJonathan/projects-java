package com.example.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.entities.PersonaJuridicaEntity;

@Repository
public interface PersonaJuridicaRepository extends CrudRepository<PersonaJuridicaEntity, Long>{

}
