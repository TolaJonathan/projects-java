package com.example.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.entities.PersonaEntity;

@Repository
public interface PersonaRepository extends CrudRepository<PersonaEntity, Long>{

}
