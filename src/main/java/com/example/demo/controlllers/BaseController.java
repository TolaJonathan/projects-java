package com.example.demo.controlllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.model.dto.ModelDTO;
import com.example.demo.model.entities.ModelEntity;


public class BaseController {

	@Autowired
	private ModelMapper modelMapper;
	
	protected <T extends ModelEntity, T2 extends ModelDTO> T2 convertToDTO(T entity, Class<T2> clazz){
		T2 mappedDTO = modelMapper.map(entity, clazz);
		return mappedDTO;
	}
	
	protected <T extends ModelDTO, T2 extends ModelEntity> T2 convertToEntity(T dto, Class<T2> clazz){
		T2 mappedEntity = modelMapper.map(dto, clazz);
		return mappedEntity;
	}
}
