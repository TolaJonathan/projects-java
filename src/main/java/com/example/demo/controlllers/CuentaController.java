package com.example.demo.controlllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.dto.CuentaDTO;
import com.example.demo.model.dto.PersonaDTO;
import com.example.demo.model.dto.PersonaJuridicaDTO;
import com.example.demo.model.dto.TitularDTO;
import com.example.demo.model.entities.CuentaEntity;
import com.example.demo.model.entities.PersonaEntity;
import com.example.demo.model.entities.PersonaJuridicaEntity;
import com.example.demo.services.CuentaService;
import com.example.demo.services.PersonaJuridicaService;
import com.example.demo.services.PersonaService;

@RestController
@RequestMapping(path = "/api")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
		RequestMethod.DELETE })
public class CuentaController extends BaseController {

	@Autowired
	private CuentaService cuentaService;

	@Autowired
	private PersonaJuridicaService personaJuridicaService;

	@Autowired
	private PersonaService personaService;

	@GetMapping("/titulares")
	public ResponseEntity<?> getTitulares() {

		List<PersonaEntity> personas = this.personaService.findAll();
		List<PersonaDTO> personaDTOList = personas.stream().map(persona -> convertToDTO(persona, PersonaDTO.class))
				.collect(Collectors.toList());

		List<PersonaJuridicaEntity> personasJuridicas = this.personaJuridicaService.findAll();
		List<PersonaJuridicaDTO> personaJuridicasDTOList = personasJuridicas.stream()
				.map(personaJuridica -> convertToDTO(personaJuridica, PersonaJuridicaDTO.class))
				.collect(Collectors.toList());

		return new ResponseEntity<>(new TitularDTO(personaDTOList, personaJuridicasDTOList), HttpStatus.OK);
	}

	@GetMapping("/cuentas")
	public List<CuentaDTO> getCuentas() {
		List<CuentaEntity> cuentas = this.cuentaService.findAll();
		List<CuentaDTO> cuentaDTOList = cuentas.stream().map(cuenta -> convertToDTO(cuenta, CuentaDTO.class))
				.collect(Collectors.toList());
		return cuentaDTOList;
	}

	@GetMapping("/cuenta/{id}")
	public CuentaDTO getCuenta(@PathVariable(value = "id") Long id) {
		Optional<CuentaEntity> cuenta = this.cuentaService.findById(id);
		CuentaDTO cuentaDTO = convertToDTO(cuenta.get(), CuentaDTO.class);
		return cuentaDTO;
	}

	@PostMapping(path = "/cuenta", consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> createCuenta(@RequestBody CuentaDTO cuentaDTO) {

		ResponseEntity<?> response = null;
		CuentaEntity cuenta = convertToEntity(cuentaDTO, CuentaEntity.class);
		CuentaEntity cuentaCreated = this.cuentaService.create(cuenta);
		CuentaDTO newCuentaDTO = convertToDTO(cuentaCreated, CuentaDTO.class);
		response = ResponseEntity.status(HttpStatus.OK).body(newCuentaDTO);
		return response;
	}

	@PutMapping("/cuenta/{id}")
	public ResponseEntity<?> updateCuenta(@PathVariable(value = "id") Long id, @RequestBody CuentaDTO cuentaDTO) {

		Map<String, Object> response = new HashMap<>();
		Optional<CuentaEntity> cuentaActual = this.cuentaService.findById(id);
		CuentaEntity cuentaUpdated = null;
		if (cuentaActual.get() == null) {
			response.put("mensaje", "Error: No se pudo editar la cuenta "
					.concat(id.toString().concat(" ya que no se encontró en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		try {
			cuentaActual.get().setNumero_cuenta(cuentaDTO.getNumero_cuenta());
			cuentaActual.get().setSaldo(cuentaDTO.getSaldo());
			cuentaActual.get().setTipo_tarjeta(cuentaDTO.getTipo_tarjeta());
			cuentaUpdated = cuentaService.create(cuentaActual.get());
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar la cuenta");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		CuentaDTO cDTO = convertToDTO(cuentaUpdated, CuentaDTO.class);
		response.put("mensaje", "La cuenta fue actualizada correctamente");
		response.put("cuenta", cDTO);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@DeleteMapping("/cuenta/{id}")
	public ResponseEntity<?> deleteCuenta(@PathVariable(value = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		try {
			this.cuentaService.deleteById(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al borrar la cuenta");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La cuenta fue eliminada");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	// ------------------------------------ Personas---------------------
	@GetMapping("/persona/{id}")
	public ResponseEntity<?> getPersona(@PathVariable(value = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		PersonaEntity persona = null;
		try {
			persona = this.personaService.findById(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "No se encontro ningun titular con el id: " + id);
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (persona == null) {
			response.put("mensaje",
					"El titular con id: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		PersonaDTO personaDTO = convertToDTO(persona, PersonaDTO.class);
		return new ResponseEntity<PersonaDTO>(personaDTO, HttpStatus.OK);
	}

	@PostMapping(path = "/persona", consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> createPersona(@Valid @RequestBody PersonaDTO personaDTO) {

		Map<String, Object> response = new HashMap<>();
		PersonaEntity personaCreated = null;
		PersonaEntity persona = convertToEntity(personaDTO, PersonaEntity.class);
		List<PersonaEntity> personas = this.personaService.findAll();
		List<PersonaJuridicaEntity> personasJ = this.personaJuridicaService.findAll();

		List<PersonaDTO> personaDTOList = personas.stream().map(per -> convertToDTO(per, PersonaDTO.class))
				.collect(Collectors.toList());
		List<PersonaJuridicaDTO> personaJDTOList = personasJ.stream()
				.map(j -> convertToDTO(j, PersonaJuridicaDTO.class)).collect(Collectors.toList());

		try {
			for (PersonaDTO p : personaDTOList) {
				if (p.getCuil().equals(persona.getCuil())) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body("Ya existe un Titular con el mismo cuil.");
				}
			}
			for (PersonaJuridicaDTO j : personaJDTOList) {
				if (j.getCuil().equals(persona.getCuil())) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body("Ya existe un Titular con el mismo cuil.");
				}
			}
			personaCreated = this.personaService.create(persona);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al crear la persona");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		PersonaDTO newPersonaDTO = convertToDTO(personaCreated, PersonaDTO.class);
		response.put("mensaje", "Titular creado con exito");
		response.put("persona", newPersonaDTO);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PutMapping("/persona/{id}")
	public ResponseEntity<?> updatePersona(@PathVariable(value = "id") Long id, @RequestBody PersonaDTO personaDTO) {

		Map<String, Object> response = new HashMap<>();
		PersonaEntity personaActual = this.personaService.findById(id);
		PersonaEntity personaUpdated = null;
		if (personaActual == null) {
			response.put("mensaje", "Error: No se pudo editar los datos de la persona "
					.concat(id.toString().concat(" ya que no se encontró en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		List<CuentaEntity> cuentas = personaDTO.getCuentas().stream().map(c -> convertToEntity(c, CuentaEntity.class))
				.collect(Collectors.toList());
		try {
			personaActual.setApellido(personaDTO.getApellido());
			personaActual.setNombre(personaDTO.getNombre());
			personaActual.setCc(personaDTO.getCc());
			personaActual.setCuil(personaDTO.getCuil());
			personaActual.setCuentas(cuentas);
			personaUpdated = personaService.create(personaActual);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar la persona");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		PersonaDTO cDTO = convertToDTO(personaUpdated, PersonaDTO.class);
		response.put("mensaje", "La datos de la persona fueron actualizados correctamente");
		response.put("persona", cDTO);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@DeleteMapping("/persona/{id}")
	public ResponseEntity<?> deletePersona(@PathVariable(value = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		try {
			this.personaService.deleteById(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al borrar la persona");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La persona fue eliminada");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	// -------------------------------------Persona
	// Juridica-------------------------------------------------------------
	@GetMapping("/persona-juridica/{id}")
	public ResponseEntity<?> getPersonaJuridica(@PathVariable(value = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		PersonaJuridicaEntity personaJuridica = null;
		try {
			personaJuridica = this.personaJuridicaService.findById(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "No se encontro ningun titular con el id: " + id);
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (personaJuridica == null) {
			response.put("mensaje",
					"El titular con id: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		PersonaJuridicaDTO personaJuridicaDTO = convertToDTO(personaJuridica, PersonaJuridicaDTO.class);
		return new ResponseEntity<PersonaJuridicaDTO>(personaJuridicaDTO, HttpStatus.OK);
	}

	@PostMapping(path = "/persona-juridica", consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> createPersonaJuridica(@Valid @RequestBody PersonaJuridicaDTO personaJuridicaDTO) {

		Map<String, Object> response = new HashMap<>();
		PersonaJuridicaEntity personaJCreated = null;
		PersonaJuridicaEntity personaJuridica = convertToEntity(personaJuridicaDTO, PersonaJuridicaEntity.class);

		List<PersonaEntity> personas = this.personaService.findAll();
		List<PersonaJuridicaEntity> personasJ = this.personaJuridicaService.findAll();

		List<PersonaDTO> personaDTOList = personas.stream().map(per -> convertToDTO(per, PersonaDTO.class))
				.collect(Collectors.toList());
		List<PersonaJuridicaDTO> personaJDTOList = personasJ.stream()
				.map(j -> convertToDTO(j, PersonaJuridicaDTO.class)).collect(Collectors.toList());

		try {
			for (PersonaDTO p : personaDTOList) {
				if (p.getCuil().equals(personaJuridica.getCuil())) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body("Ya existe un Titular con el mismo cuil.");
				}
			}
			for (PersonaJuridicaDTO j : personaJDTOList) {
				if (j.getCuil().equals(personaJuridica.getCuil())) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body("Ya existe un Titular con el mismo cuil.");
				}
			}
			personaJCreated = this.personaJuridicaService.create(personaJuridica);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al crear la Persona Juridica");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		PersonaJuridicaDTO newPersonaJuridicaDTO = convertToDTO(personaJCreated, PersonaJuridicaDTO.class);
		response.put("mensaje", "Titular creado con exito");
		response.put("persona_juridica", newPersonaJuridicaDTO);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PutMapping("/persona-juridica/{id}")
	public ResponseEntity<?> updatePersonaJuridica(@PathVariable(value = "id") Long id,
			@RequestBody PersonaJuridicaDTO personaJuridicaDTO) {

		Map<String, Object> response = new HashMap<>();
		PersonaJuridicaEntity personaJuridicaActual = this.personaJuridicaService.findById(id);
		PersonaJuridicaEntity personaJuridicaUpdated = null;
		if (personaJuridicaActual == null) {
			response.put("mensaje", "Error: No se pudo editar los datos de la personaJuridica "
					.concat(id.toString().concat(" ya que no se encontró en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		List<CuentaEntity> cuentas = personaJuridicaDTO.getCuentas().stream()
				.map(c -> convertToEntity(c, CuentaEntity.class)).collect(Collectors.toList());
		try {
			personaJuridicaActual.setCuil(personaJuridicaDTO.getCuil());
			personaJuridicaActual.setRazon_social(personaJuridicaDTO.getRazon_social());
			personaJuridicaActual.setFechaCreacion(personaJuridicaDTO.getFecha_creacion());
			personaJuridicaActual.setCuentas(cuentas);
			personaJuridicaUpdated = personaJuridicaService.create(personaJuridicaActual);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar la Persona Juridica");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		PersonaJuridicaDTO cDTO = convertToDTO(personaJuridicaUpdated, PersonaJuridicaDTO.class);
		response.put("mensaje", "La datos de la Persona Juridica fueron actualizados correctamente");
		response.put("persona_juridica", cDTO);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@DeleteMapping("/persona-juridica/{id}")
	public ResponseEntity<?> deletePersonaJuridica(@PathVariable(value = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		try {
			this.personaJuridicaService.deleteById(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al borrar la Persona Juridica");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La Persona Juridica fue eliminada");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
