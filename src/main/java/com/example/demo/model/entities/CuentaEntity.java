package com.example.demo.model.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CUENTAS")
public class CuentaEntity extends ModelEntity{
	
	private static final long serialVersionUID = 8382507824484393466L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "numero_cuenta")
	private Long numero_cuenta;
	
	@Column(name = "saldo")
	private Double saldo;
	
	@Column(name = "tipo_tarjeta")
	private String tipo_tarjeta;
	
	@ManyToMany(mappedBy = "cuentas", cascade = CascadeType.ALL)
	private List<PersonaEntity> personas;
	
	@ManyToMany(mappedBy = "cuentas", cascade = CascadeType.ALL)
	private List<PersonaJuridicaEntity> personasJuridicas;

	public Long getNumero_cuenta() {
		return numero_cuenta;
	}

	public void setNumero_cuenta(Long numero_cuenta) {
		this.numero_cuenta = numero_cuenta;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public String getTipo_tarjeta() {
		return tipo_tarjeta;
	}

	public void setTipo_tarjeta(String tipo_tarjeta) {
		this.tipo_tarjeta = tipo_tarjeta;
	}

	public List<PersonaEntity> getPersonas() {
		return personas;
	}

	public void setPersonas(List<PersonaEntity> personas) {
		this.personas = personas;
	}

	public List<PersonaJuridicaEntity> getPersonasJuridicas() {
		return personasJuridicas;
	}

	public void setPersonasJuridicas(List<PersonaJuridicaEntity> personasJuridicas) {
		this.personasJuridicas = personasJuridicas;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

}