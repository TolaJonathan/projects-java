package com.example.demo.model.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "PERSONAS_JURIDICAS")
public class PersonaJuridicaEntity extends ModelEntity{

	private static final long serialVersionUID = -6487078300064339681L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Size(max = 100)
	@Column(name = "razon_social")
	private String razon_social;
	
	@DateTimeFormat(pattern = "yyyy-mm-dd")
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;
	
	@NotNull
	@Column(name = "cuil")
	private String cuil;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "personas_juridicas_cuentas",
	joinColumns = @JoinColumn(name = "persona_juridica_id"),
	inverseJoinColumns = @JoinColumn(name = "cuenta_id"))
	private List<CuentaEntity> cuentas;

	public String getRazon_social() {
		return razon_social;
	}

	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getCuil() {
		return cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}

	public List<CuentaEntity> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<CuentaEntity> cuentas) {
		this.cuentas = cuentas;
	}

	public Long getId() {
		return id;
	}
	
}
