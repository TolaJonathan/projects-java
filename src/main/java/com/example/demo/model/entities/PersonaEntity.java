package com.example.demo.model.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.Table;

@Entity
@Table(name = "PERSONAS")
public class PersonaEntity extends ModelEntity{

	private static final long serialVersionUID = -5571403360629938767L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Size(max = 80)
	@Column(name = "nombre")
	private String nombre;
	
	@Size(max = 250)
	@Column(name = "apellido")
	private String apellido;
	
	@Column(name = "cc")
	private Long cc;
	
	@NotNull
	@Column(name = "cuil")
	private String cuil;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "personas_cuentas",
	joinColumns = @JoinColumn(name = "persona_id"),
	inverseJoinColumns = @JoinColumn(name = "cuenta_id"))
	private List<CuentaEntity> cuentas;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Long getCc() {
		return cc;
	}

	public void setCc(Long cc) {
		this.cc = cc;
	}

	public String getCuil() {
		return cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}

	public List<CuentaEntity> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<CuentaEntity> cuentas) {
		this.cuentas = cuentas;
	}

	public Long getId() {
		return id;
	}

}
