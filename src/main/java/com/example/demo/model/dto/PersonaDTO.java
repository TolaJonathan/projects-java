package com.example.demo.model.dto;

import java.util.List;

public class PersonaDTO extends ModelDTO{
	
	private static final long serialVersionUID = -7791735009749120387L;

	private Long id;
	
	private String nombre;
	
	private String apellido;
	
	private Long cc;
	
	private String cuil;

	private List<CuentaDTO> cuentas;
	
	public List<CuentaDTO> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<CuentaDTO> cuentas) {
		this.cuentas = cuentas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Long getCc() {
		return cc;
	}

	public void setCc(Long cc) {
		this.cc = cc;
	}

	public String getCuil() {
		return cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}
}
