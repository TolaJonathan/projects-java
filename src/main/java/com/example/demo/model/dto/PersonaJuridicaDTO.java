package com.example.demo.model.dto;

import java.util.Date;
import java.util.List;


public class PersonaJuridicaDTO extends ModelDTO{
	
	private static final long serialVersionUID = -5095477597574489827L;

	private Long id;
	
	private String razon_social;
	
	private Date fecha_creacion;
	
	private String cuil;
	
	private List<CuentaDTO> cuentas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRazon_social() {
		return razon_social;
	}

	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}

	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public String getCuil() {
		return cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}

	public List<CuentaDTO> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<CuentaDTO> cuentas) {
		this.cuentas = cuentas;
	}

}
