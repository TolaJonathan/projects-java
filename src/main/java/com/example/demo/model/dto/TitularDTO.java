package com.example.demo.model.dto;

import java.util.List;

public class TitularDTO extends ModelDTO{

	private static final long serialVersionUID = 5554582895808680913L;

	private List<PersonaDTO> personas;
	private List<PersonaJuridicaDTO> personas_juridicas;
	
	public TitularDTO(List<PersonaDTO> personas, List<PersonaJuridicaDTO> personas_juridicas) {
		super();
		this.personas = personas;
		this.personas_juridicas = personas_juridicas;
	}
	
	public List<PersonaDTO> getPersonas() {
		return personas;
	}
	public void setPersonas(List<PersonaDTO> personas) {
		this.personas = personas;
	}
	public List<PersonaJuridicaDTO> getPersonas_juridicas() {
		return personas_juridicas;
	}
	public void setPersonas_juridicas(List<PersonaJuridicaDTO> personas_juridicas) {
		this.personas_juridicas = personas_juridicas;
	}
	
	
	
}
