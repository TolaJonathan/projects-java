package com.example.demo.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CuentaDTO extends ModelDTO {

	private static final long serialVersionUID = -6205884491329739292L;

	private Long id;

	private Long numero_cuenta;
	
	private Double saldo;
	
	private String tipo_tarjeta;
	
	@JsonIgnore
	private List<PersonaDTO> personas;
	
	@JsonIgnore
	private List<PersonaJuridicaDTO>  personas_juridicas;

	public CuentaDTO() {}
	
	public CuentaDTO(Long numero_cuenta, Double saldo, String tipo_tarjeta, List<PersonaDTO> personas,
			List<PersonaJuridicaDTO> personas_juridicas) {
		super();
		this.numero_cuenta = numero_cuenta;
		this.saldo = saldo;
		this.tipo_tarjeta = tipo_tarjeta;
		this.personas = personas;
		this.personas_juridicas = personas_juridicas;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Long getNumero_cuenta() {
		return numero_cuenta;
	}

	public void setNumero_cuenta(Long numero_cuenta) {
		this.numero_cuenta = numero_cuenta;
	}

	public String getTipo_tarjeta() {
		return tipo_tarjeta;
	}

	public void setTipo_tarjeta(String tipo_tarjeta) {
		this.tipo_tarjeta = tipo_tarjeta;
	}

	
}
