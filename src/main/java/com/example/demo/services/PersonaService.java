package com.example.demo.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.entities.PersonaEntity;

@Service
public interface PersonaService {

	public PersonaEntity findById(Long id);
	public List<PersonaEntity> findAll();
	public PersonaEntity create(PersonaEntity persona);
	public void deleteById(Long id);
}
