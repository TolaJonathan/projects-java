package com.example.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.model.entities.CuentaEntity;

@Service
public interface CuentaService {

	public List<CuentaEntity> findAll();
	public Optional<CuentaEntity> findById(Long id);
	public CuentaEntity create(CuentaEntity cuenta);
	public void deleteById(Long id);
}
