package com.example.demo.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.entities.PersonaJuridicaEntity;
import com.example.demo.repositories.PersonaJuridicaRepository;
import com.example.demo.services.PersonaJuridicaService;

@Service
public class PersonaJuridicaServiceImpl implements PersonaJuridicaService{

	@Autowired
	private PersonaJuridicaRepository personaJuridicaRepository; 

	@Override
	public PersonaJuridicaEntity findById(Long id) {
		return this.personaJuridicaRepository.findById(id).orElse(null);
	}
	
	@Override
	public List<PersonaJuridicaEntity> findAll() {
		return (List<PersonaJuridicaEntity>) this.personaJuridicaRepository.findAll();
	}

	@Override
	public PersonaJuridicaEntity create(PersonaJuridicaEntity personaJuridica) {
		return this.personaJuridicaRepository.save(personaJuridica);
	}

	@Override
	public void deleteById(Long id) {
		this.personaJuridicaRepository.deleteById(id);
	}

}
