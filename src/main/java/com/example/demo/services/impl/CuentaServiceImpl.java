package com.example.demo.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.entities.CuentaEntity;
import com.example.demo.repositories.CuentaRepository;
import com.example.demo.services.CuentaService;

@Service
public class CuentaServiceImpl implements CuentaService{
	
	@Autowired
	private CuentaRepository cuentaRepository;

	@Override
	public Optional<CuentaEntity> findById(Long id) {
		return this.cuentaRepository.findById(id);
	}
	
	@Override
	public List<CuentaEntity> findAll() {
		return (List<CuentaEntity>) this.cuentaRepository.findAll();
	}

	@Override
	public CuentaEntity create(CuentaEntity cuenta) {
		return this.cuentaRepository.save(cuenta);
	}


	@Override
	public void deleteById(Long id) {
		this.cuentaRepository.deleteById(id);
	}
	
}
