package com.example.demo.services.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.entities.PersonaEntity;
import com.example.demo.repositories.PersonaRepository;
import com.example.demo.services.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	private PersonaRepository personaRepository;

	@Override
	public PersonaEntity findById(Long id) {
		return this.personaRepository.findById(id).orElse(null);
	}
	
	@Override
	public List<PersonaEntity> findAll() {
		return (List<PersonaEntity>) this.personaRepository.findAll();
	}

	@Override
	public PersonaEntity create(PersonaEntity persona) {
		return this.personaRepository.save(persona);
	}

	@Override
	public void deleteById(Long id) {
		this.personaRepository.deleteById(id);
	}

}
