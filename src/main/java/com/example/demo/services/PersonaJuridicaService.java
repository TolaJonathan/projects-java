package com.example.demo.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.entities.PersonaJuridicaEntity;

@Service
public interface PersonaJuridicaService {
	
	public PersonaJuridicaEntity findById(Long id);
	public List<PersonaJuridicaEntity> findAll();
	public PersonaJuridicaEntity create(PersonaJuridicaEntity personaJuridica);
	public void deleteById(Long id);
	
}
